package main

import (
    "fmt"
    "os"
    "strings"
    "html/template"
    "net/http"

    "github.com/anaskhan96/soup"
)

type ingredients_table struct {
    Title string
    Ingredients []string
}


type recipe struct {
    Title string
    Description string
    Preptime string
    Difficulty string
    Date string
    Kcalories string
    ImageURI string

    Ratings string
    RatingsAverage string

    Ingredients []ingredients_table

    Instructions string
}

type search_result struct {
    Title string
    ImageURI string
    URI string
}

func search(search_term string) []search_result {
    resp, err := soup.Get("https://chefkoch.de/rs/s0/" + search_term + "/Rezepte.html");
    if err != nil {
        fmt.Println(err);
        //TODO: do not do this
        os.Exit(1);
    }

    doc := soup.HTMLParse(resp);

    search_results := doc.Find("div", "class", "recipe-list").FindAll("div", "class", "ds-recipe-card");

    var results []search_result;
    
    for _, res := range search_results {
        var result search_result;

        result.Title = strings.TrimSpace(res.Find("h2", "class", "ds-recipe-card__headline").Text());
        result.URI = strings.TrimPrefix(res.Find("a").Attrs()["href"], "https://www.chefkoch.de");
        result.ImageURI = res.Find("img").Attrs()["src"];

        results = append(results, result);
    }

    return results;
}

func get_recipe(url string) recipe {
    var r recipe;

    resp, err := soup.Get(url);
    if err != nil {
        fmt.Println(err);
        //TODO: do not do this
        os.Exit(1);
    }

    doc := soup.HTMLParse(resp);

    //////////////
    // METADATA //
    //////////////

    r.Title = doc.Find("h1").Text();
    r.Description = strings.TrimSpace(doc.Find("p", "class", "recipe-text").Text());
    r.Preptime = strings.TrimSpace(doc.Find("span", "class", "recipe-preptime").Text());
    r.Difficulty = strings.TrimSpace(doc.Find("span", "class", "recipe-difficulty").Text());
    r.Date = strings.TrimSpace(doc.Find("span", "class", "recipe-date").Text());

    r.Ratings = strings.TrimSpace(doc.Find("div", "class", "ds-rating-count").FullText())
    r.RatingsAverage = strings.TrimSpace(doc.Find("div", "class", "ds-rating-avg").Find("strong").Text())

    kcalories_element := doc.Find("span", "class", "recipe-kcalories");
    if kcalories_element.Error == nil {
        r.Kcalories = strings.TrimSpace(kcalories_element.Text());
    }

    r.ImageURI = doc.Find("img", "class", "i-amphtml-fill-content").Attrs()["src"];

    /////////////////
    // INGREDIENTS //
    /////////////////

    ingredients_tables := doc.FindAll("table", "class", "ingredients");

    for _, ingredients_tab := range ingredients_tables {
        table_name_el := ingredients_tab.Find("h3");

        var ingreds ingredients_table

        if table_name_el.Error == nil {
            ingreds.Title = table_name_el.Text();
        }

        var ingredients []string

        for _, row := range ingredients_tab.Find("tbody").FindAll("tr") {
            ingredient_str := "";

            for idx, column := range row.FindAll("td") {
                if (idx == 0) {
                    ingredient_str = strings.Join(strings.Fields(column.FullText()), " ");
                    if (len(ingredient_str) > 0) {
                        // add this space so the next time where the name of the ingredient is added
                        // it will be added with a space in between
                        ingredient_str += " ";
                    }
                } else if (idx == 1) {
                    ingredient_str += strings.TrimSpace(column.FullText());
                }
            }
            ingredients = append(ingredients, ingredient_str)
        }

        ingreds.Ingredients = ingredients
        r.Ingredients = append(r.Ingredients, ingreds)
    }


    //////////////////
    // INSTRUCTIONS //
    //////////////////

    articles := doc.FindAll("article", "class", "ds-box");

    for _, article := range articles {
        section := article.Find("h2");

        if (section.Error != nil) {
            continue;
        }
       
        section_title := section.Attrs()["data-vars-tracking-title"];
        
        if (section_title == "Zubereitung") {
            r.Instructions = strings.TrimSpace(article.Find("div", "class", "ds-box").FullText());

            // We're done finding the instructions, we can quit
            break;
        }
    }

    return r;
}

func main() {
    fmt.Println("Serving on :8080")

    rezept_template, rezept_err := template.ParseFiles("templates/rezept.html");
    suchergebnis_template, suchergebnis_err := template.ParseFiles("templates/suchergebnis.html");

    if (rezept_err != nil) {
        panic(rezept_err);
    }
    if (suchergebnis_err != nil) {
        panic(suchergebnis_err);
    }

    http.HandleFunc("/rezepte/", func(w http.ResponseWriter, r *http.Request) {
        rec := get_recipe("https://chefkoch.de" + r.URL.Path);
        ret := rezept_template.Execute(w, rec);

        // we only want to print if there is an error
        if ret != nil {
            fmt.Println(ret);
        }

        /*
        fmt.Println("Title: " + rec.Title);
        fmt.Println("Description: " + rec.Description);
        fmt.Println("Preptime: " + rec.Preptime);
        fmt.Println("Difficulty: " + rec.Difficulty);
        fmt.Println("Date: " + rec.Date);
        fmt.Println("Kcalories: " + rec.Kcalories);
        fmt.Println("ImageURI: " + rec.ImageURI);
        fmt.Println("Ratings: " + rec.Ratings);
        fmt.Println("Ratings Average: " + rec.RatingsAverage);

        for _, ingredients := range rec.Ingredients {
            fmt.Println(ingredients.Title);
            fmt.Println(ingredients.Ingredients);
        }
        */
    })

    http.HandleFunc("/rs/s0/", func(w http.ResponseWriter, r *http.Request) {
        //TODO: make this much more stable, it will fail once it will use a /Magazine.html suffix
        path := strings.TrimSuffix(strings.TrimPrefix(r.URL.Path, "/rs/s0/"), "/Rezepte.html");
        results := search(path);

        ret := suchergebnis_template.Execute(w, results);

        // we only want to print if there is an error
        if ret != nil {
            fmt.Println(ret);
        }
    })
    
    fmt.Println(http.ListenAndServe(":8080", nil));
}
